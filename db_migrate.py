import imp
from migrate.versioning import api
from app import db, app
from app.models import User, ROLE_ADMIN, Group
migration = app.config['SQLALCHEMY_MIGRATE_REPO'] + '/versions/%03d_migration.py' % (api.db_version(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO']) + 1)
tmp_module = imp.new_module('old_model')
old_model = api.create_model(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])
#exec old_model in tmp_module.__dict__
exec(old_model, tmp_module.__dict__)
script = api.make_update_script_for_model(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'], tmp_module.meta, db.metadata)
open(migration, "wt").write(script)
api.upgrade(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])

#u = User(username = "Admin", email = "admin@admin.com", password="admin", role = ROLE_ADMIN)
#u.set_password("admin")
#g = Group(group_name = "Без группы")
#db.session.add(u)
#db.session.add(g)
#db.session.commit()

print("New migration saved as " + migration)
print('Current database version: ' + str(api.db_version(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])))