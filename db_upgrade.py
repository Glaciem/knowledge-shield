from migrate.versioning import api
from app import app
v = api.db_version(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])
api.upgrade(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'], v + 1)

from app import db
from app.models import User, ROLE_ADMIN

u = User(username = "Admin", email = "admin@admin.com", password="admin", role = ROLE_ADMIN)
u.set_password("admin")
db.session.add(u)
db.session.commit()

print('Current database version: ' + str(api.db_version(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])))