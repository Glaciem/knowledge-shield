from app import app, db, login_manager
#from datetime import datetime
import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin

import jwt

ROLE_CLIENT = 0
ROLE_USER = 1
ROLE_ADMIN = 2

@login_manager.user_loader
def load_user(user_id):
    return db.session.query(User).get(user_id)

class User(db.Model, UserMixin):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(120), index=True, unique=True)
	password = db.Column(db.String(128))
	first_name = db.Column(db.Text(), index=True, unique=False)
	second_name = db.Column(db.Text(), index=True, unique=False)
	third_name = db.Column(db.Text(), index=True, unique=False)

	role = db.Column(db.SmallInteger, index=True, default = ROLE_CLIENT)
	group_id = db.Column(db.Integer(), db.ForeignKey('groups.id'), default = 1)

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	courses_progress = db.relationship('CourseProgress', backref='users')
	fish_attack_option = db.relationship('FishAttackOption', backref='users', uselist=False)
	fish_attack = db.relationship('FishAttack', backref='users', uselist=False)
	click_stats = db.relationship('ClickStat', backref='users')
	tests_results = db.relationship('TestResult', backref='users')
	appointments = db.relationship('Appointment', backref='users')
	
	

	def __repr__(self):
		return '<User {} - {}>'.format(self.username, self.id)

	def set_password(self, password):
		self.password = generate_password_hash(password)

	def check_password(self,  password):
		return check_password_hash(self.password, password)

class Group(db.Model, UserMixin):
	__tablename__ = 'groups'
	id = db.Column(db.Integer, primary_key=True)
	group_name = db.Column(db.String(64), index=True, unique=True)

	users = db.relationship('User', backref='groups')
	fish_attack = db.relationship('FishAttack', backref='groups', uselist=False)

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	def __repr__(self):
		return '<Group {} - {}>'.format(self.group_name, self.id)


class CourseProgress(db.Model, UserMixin):
	__tablename__ = 'courses_progress'
	id = db.Column(db.Integer, primary_key=True)
	course_id = db.Column(db.Integer(), db.ForeignKey('courses.id'))
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	progress = db.Column(db.Text(), unique=False, default = "{}")

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	def __repr__(self):
		return '<Course {} - {}>'.format(self.course_title, self.id)

course_coursegroup = db.Table('course_coursegroup',
	db.Column('course_id', db.Integer, db.ForeignKey('courses.id')),
	db.Column('coursegroup_id', db.Integer, db.ForeignKey('courses_groups.id'))
)

class Course(db.Model, UserMixin):
	__tablename__ = 'courses'
	id = db.Column(db.Integer, primary_key=True)
	course_title = db.Column(db.String(64), index=True, unique=True)
	description = db.Column(db.Text(), unique=False)
	structure = db.Column(db.Text(), unique=False, default = "{}")

	courses_progress = db.relationship('CourseProgress', backref='courses')
	course_groups = db.relationship('CourseGroup', secondary=course_coursegroup, backref='courses')
	
	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	def __repr__(self):
		return '<Course {} - {}>'.format(self.course_title, self.id)

class CourseGroup(db.Model, UserMixin):
	__tablename__ = 'courses_groups'
	id = db.Column(db.Integer, primary_key=True)
	group_title = db.Column(db.String(64), index=True, unique=True)
	description = db.Column(db.Text(), unique=False)
	structure = db.Column(db.Text(), unique=False, default="[]")

	#courses = db.relationship('Course', secondary=course_coursegroup, backref='courses_groups')
	
	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	test = db.relationship('Test', backref='course_group', uselist=False)


	def __repr__(self):
		return '<CourseGroup {} - {}>'.format(self.group_title, self.id)

mail_attackoption = db.Table('mail_attackoption',
	db.Column('attack_option_id', db.Integer, db.ForeignKey('fish_attack_options.id')),
	db.Column('mail_id', db.Integer, db.ForeignKey('fish_mails.id'))
)

class FishAttackOption(db.Model, UserMixin):
	__tablename__ = 'fish_attack_options'
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	logo = db.Column(db.Text(), unique=False)
	inbox = db.Column(db.Text(), index=True, unique=False, default="company@company.ru")
	company_name = db.Column(db.Text(), index=True, unique=False, default="Company")
	#mails = db.Column(db.Text(), index=True, unique=False, default="[]")
	mails_count = db.Column(db.Integer(), default=0)
	delay = db.Column(db.String(64), default="1:0:0")

	mails = db.relationship('FishMail', secondary=mail_attackoption, backref='fish_attack_options')

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	def __repr__(self):
		return '<FishAttackOption {} - {}>'.format(self.user_id, self.id)

class FishAttack(db.Model, UserMixin):
	__tablename__ = 'fish_attacks'
	id = db.Column(db.Integer, primary_key=True)
	attack_title = db.Column(db.Text(), index=True, unique=False, default = "Без названия")
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	user_group_id = db.Column(db.Integer(), db.ForeignKey('groups.id'))
	start_time = db.Column(db.DateTime(), index=True, default=datetime.datetime.utcnow)
	end_time = db.Column(db.DateTime(), index=True)
	last_mail_time = db.Column(db.DateTime(), index=True)
	option = db.Column(db.Text())
	status = db.Column(db.Integer(), index=True, unique=False, default = 1)
	
	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	click_stats = db.relationship('ClickStat', backref='fish_attacks')

	def __repr__(self):
		return '<FishAttack {} - {}>'.format(self.attack_title, self.id)

class FishMail(db.Model, UserMixin):
	__tablename__ = 'fish_mails'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.Text(), index=True, unique=False)
	subject = db.Column(db.Text(), index=True, unique=False)
	mail_name = db.Column(db.Text(), index=True, unique=False)
	mail_from = db.Column(db.Text(), index=True, unique=False)
	file_name = db.Column(db.Text(), index=True, unique=False)

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	click_stats = db.relationship('ClickStat', backref='fish_mails')

	def __repr__(self):
		return '<FishMail {} - {}>'.format(self.title, self.id)


	def get_fish_token(self, user_id, attack_id):
		return jwt.encode(
			{'mail_id': self.id, "user_id": user_id, "attack_id": attack_id},
			app.config['SECRET_KEY'], algorithm='HS256')#.decode('utf-8')

	@staticmethod
	def verify_token(token):
		try:
			user_id = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['user_id']
			attack_id = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['attack_id']
			mail_id = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])['mail_id']
		except:
			return
		return (User.query.get(user_id), FishAttack.query.get(attack_id), FishMail.query.get(mail_id))


class ClickStat(db.Model, UserMixin):
	__tablename__ = 'click_stats'
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	attack_id = db.Column(db.Integer(), db.ForeignKey('fish_attacks.id'))
	mail_id = db.Column(db.Integer(), db.ForeignKey('fish_mails.id'))
	
	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	def __repr__(self):
		return '<ClickStat {}>'.format(self.id)

# test_coursegroup = db.Table('test_coursegroup',
# 	db.Column('test_id', db.Integer, db.ForeignKey('tests.id')),
# 	db.Column('coursegroup_id', db.Integer, db.ForeignKey('courses_groups.id'))
# )

class Test(db.Model, UserMixin):
	__tablename__ = 'tests'
	id = db.Column(db.Integer, primary_key=True)
	test_title = db.Column(db.Text(), index=True, unique=True)
	group_id = db.Column(db.Integer(), db.ForeignKey('courses_groups.id'))
	structure = db.Column(db.Text(), unique=False, default = "{}")
	porog = db.Column(db.Integer(), unique=False)
	max_ans = db.Column(db.Integer(), unique=False)

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	# test_groups = db.relationship('CourseGroup', secondary=test_coursegroup, backref='tests')
	tests_results = db.relationship('TestResult', backref='tests')

	def __repr__(self):
		return '<Test {}>'.format(self.id)

class TestResult(db.Model, UserMixin):
	__tablename__ = 'tests_results'
	id = db.Column(db.Integer, primary_key=True)
	test_id = db.Column(db.Integer(), db.ForeignKey('tests.id'))
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	group_id = db.Column(db.Integer(), db.ForeignKey('courses_groups.id'))
	correct = db.Column(db.Integer(), unique=False)
	result = db.Column(db.Integer(), index=True, unique=False)
	structure = db.Column(db.Text(), unique=False)

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	# test_groups = db.relationship('CourseGroup', secondary=test_coursegroup, backref='tests')


	def __repr__(self):
		return '<TestResult {}>'.format(self.id)

appointment_course = db.Table('appointment_course',
	db.Column('appointment_id', db.Integer, db.ForeignKey('appointments.id')),
	db.Column('course_id', db.Integer, db.ForeignKey('courses.id'))
)

appointment_coursegroup = db.Table('appointment_coursegroup',
	db.Column('appointment_id', db.Integer, db.ForeignKey('appointments.id')),
	db.Column('course_group_id', db.Integer, db.ForeignKey('courses_groups.id'))
)

class Appointment(db.Model, UserMixin):
	__tablename__ = 'appointments'
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
	deadline = db.Column(db.DateTime())

	created_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow)
	updated_on = db.Column(db.DateTime(), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

	courses = db.relationship('Course', secondary=appointment_course, backref='appointments')
	groups = db.relationship('CourseGroup', secondary=appointment_coursegroup, backref='appointments')

	def __repr__(self):
		return '<Appointment {}>'.format(self.id)

