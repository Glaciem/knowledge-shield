from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail, Message

import os


basedir = os.path.abspath(os.path.dirname(__file__))



app = Flask(__name__)


app.config['SERVER_NAME'] = '127.0.0.1:5000'
app.config['SECRET_KEY'] = '00900090SuperKey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_MIGRATE_REPO'] = os.path.join(basedir, 'db_repository')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
#app.config['MAIL_SERVER'] = 'smtp.yandex.ru'

app.config['MAIL_PORT'] = 465

#app.config['MAIL_USE_TLS'] = True  
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'test0090g@gmail.com'
app.config['MAIL_DEFAULT_SENDER'] = 'test0090g@gmail.com'
#app.config['MAIL_USERNAME'] = 'test0090g@yandex.ru'
#app.config['MAIL_DEFAULT_SENDER'] = 'test0090g@yandex.ru'

app.config['MAIL_PASSWORD'] = '1234567890test'

#imap_host = "imap.gmail.com"
#smtp_host = "smtp.gmail.com"
#imap_port = 993
#smtp_port = 465
#user = "test0090g@gmail.com"
#passwd = "1234567890test"
#from_mail = "0090test@mail.ru"
#admin_mail = "minchenko-pervak@vtb.ru"

toolbar = DebugToolbarExtension(app)
db = SQLAlchemy(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
mail = Mail(app)


from app import routes, models #, scripts