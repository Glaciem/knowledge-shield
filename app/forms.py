from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, BooleanField, SelectField, DateTimeField
from wtforms.fields.html5 import DateTimeLocalField
from wtforms.validators import DataRequired, Email, InputRequired, Required
from app import db
from app.models import Group
import datetime

class LoginForm(FlaskForm):
	username = StringField("Login", validators=[DataRequired()])
	password = StringField("Пароль", validators=[DataRequired()])
	remember = BooleanField("Запомнить меня")
	submit = SubmitField("Вход")

class AddUserForm(FlaskForm):
	#groups_db = db.session.query(Group).all()
	#groups = []
	#for group in groups_db:
	#	groups.append((group.id, group.group_name))
	#groups = [(1, "Без группы")]
	username = StringField("Логин", validators=[DataRequired()])
	#password = StringField("Password", validators=[DataRequired()])
	email = StringField("Почта", validators=[Email()])
	first_name = StringField("Имя", validators=[DataRequired()])
	second_name = StringField("Фамилия", validators=[DataRequired()])
	third_name = StringField("Отчество", validators=[DataRequired()])
	group = SelectField(
		'Группа',
		#choices=groups
		coerce=int
	)
	submit = SubmitField("Добавить")

class AddGroupForm(FlaskForm):
	name = StringField("Имя", validators=[DataRequired()])
	submit = SubmitField("Добавить")

class AddCourseGroupForm(FlaskForm):
	name = StringField("Имя", validators=[DataRequired()])
	description = TextAreaField("Описание", validators=[DataRequired()])
	submit = SubmitField("Добавить")

class StartFishAttackForm(FlaskForm):
	username = StringField("Пользователь")
	title = StringField("Название", validators=[DataRequired()])
	group = SelectField(
		'Группа',
		coerce=int
	)
	start_time = DateTimeLocalField(label='Начало', validators=[DataRequired()], format = "%Y-%m-%dT%H:%M")
	end_time = DateTimeLocalField(label='Конец', validators=[DataRequired()], format = "%Y-%m-%dT%H:%M")
	submit = SubmitField("Начать атаку")