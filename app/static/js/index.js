
$(document).ready(
  get_stat(0)
); 

function get_stat(user_id) {
  $.ajax({
    type: "GET",
    url: "/api/get_inc_stat",
    dataType: "json",
    data: {"user_id": user_id}
  }).done(function(msg) {
    labels = []
    count = []
    msg["data"].forEach(function(el) {
      labels.push(el["date"])
      count.push(el["count"])
    });
    const data = {
      labels: labels,
      datasets: [{
        label: 'Инциденты',
        data: count,
        fill: false,
        borderColor: 'rgb(255, 99, 132)',
        tension: 0.1
      }]
    };

    const config = {
      type: 'line',
      data: data,
    };
    
    console.log(labels, count)
    var myChart2 = new Chart(
      document.getElementById('myChart2'),
      config
    );

  });
  $.ajax({
    type: "GET",
    url: "/api/get_inc_stat_shape",
    dataType: "json"
  }).done(function(msg) {
      const data = {
    labels: [
      'Попались на атаку',
      'Защитились от атаки',
      'Не учавствовали в атаке'
    ],
    datasets: [{
      label: 'Сотрудники',
      data: [msg["users_attacked_count"], msg["users_protected_count"], msg["users_not_attacked_count"]],
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(54, 162, 235)',
        'rgb(255, 205, 86)'
      ],
      hoverOffset: 4
    }]
  };
  const config = {
      type: 'doughnut',
      data: data,
  };
   var myChart = new Chart(
      document.getElementById('myChart'),
      config
    );

  });
}