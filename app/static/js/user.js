const data = {
  labels: ["Январь", "Февраль", "Март", "Апрель"],
  datasets: [{
    label: 'Кол-во инцидентов',
    data: [40, 30, 20, 10],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(255, 159, 64, 0.2)',
      'rgba(255, 205, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)'
    ],
    borderColor: [
      'rgb(255, 99, 132)',
      'rgb(255, 159, 64)',
      'rgb(255, 205, 86)',
      'rgb(75, 192, 192)'
    ],
    borderWidth: 1
  }]
};

$(document).ready(
  get_stat($("#data").attr("data-userid"))
); 

function get_stat(user_id) {
  $.ajax({
    type: "GET",
    url: "/api/get_inc_stat",
    dataType: "json",
    data: {"user_id": user_id}
  }).done(function(msg) {
    labels = []
    count = []
    msg["data"].forEach(function(el) {
      labels.push(el["date"])
      count.push(el["count"])
    })
    const data = {
      labels: labels,
      datasets: [{
        label: 'Инциденты',
        data: count,
        fill: false,
        borderColor: 'rgb(255, 99, 132)',
        tension: 0.1
      }]
    };

    const config = {
      type: 'line',
      data: data,
    };
    
    var myChart2 = new Chart(
      document.getElementById('myChart2'),
      config
    );

  });
}