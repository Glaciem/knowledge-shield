
$(document).ready(
	get_page($("#data").attr("data-courseid"), $("#data").attr("data-page"))
); 

function get_page(id, page) {
	$.ajax({
		type: "GET",
		url: "/api/get_course_page",
		dataType: "json",
		data: {"course_id": id, "page": page}
	}).done(function(msg) {
		$("#main_course_block").append(msg);
	});
}

function next_button(el){
	if (el.value == "next_block"){
		//$("#data").attr("")
		$('#second_block').show();
	}else{
		$(".course-msg-block").remove();
		$("#data").attr("data-page", String(Number.parseInt($("#data").attr("data-page")) + 1));
		get_page($("#data").attr("data-courseid"), $("#data").attr("data-page"))
	}
}

function end_course(){
	$.ajax({
		type: "POST",
		url: "/api/end_course",
		contentType: 'application/json',
		dataType: "json",
		data: JSON.stringify({"course_id": Number.parseInt($("#data").attr("data-courseid"))})
	}).done(function(msg) {
		//$("#main_course_block").append(msg);
		window.location.replace("/courses");
	});
}