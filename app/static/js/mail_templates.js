$(document).ready(
	get_mail($("#data").attr("data-firstid"))
);

function get_mail(id) {
	if ($("#body_"+id).html() == "\n				    "){
		$.ajax({
			type: "GET",
			url: "/api/get_fish_mail",
			dataType: "json",
			data: {"mail_id": id}
		}).done(function(msg) {
			$("#body_"+id).append(msg["html"]);
		});	
	}
}