
function add_ans(el){
	ques = $(el).attr("data-ques")
	ans = $(el).attr("data-ans")
	next_ques = String(Number.parseInt(ques) + 1);
	next_ans = String(Number.parseInt(ans) + 1);

	str = '<div class="row"><div class="col-1 in-items-center">'+
		'<strong>'+ans+'</strong></div><div class="col"><div class="wrap-input100 m-b-16">'+
		'<input class="input100 anser" type="text" name="ques'+ques+'_ans'+ans+'" placeholder="Ответ" required><span class="focus-input100"></span>'+
		'</div></div><div class="col-1 in-items-center">'+
		'<input name="ques'+ques+'_rightans" type="radio" value="ans'+ans+'"></div></div>'
	
	btn = '<button type="button" name="ques'+ques+'_addans" id="ques'+ques+'_addans_btn" onclick="add_ans(this);" class="btn btn-secondary my-std-btn in-items-center"'+
		'data-ques="'+ques+'" data-ans="'+next_ans+'">Добавить ответ</button>'

	$("#ques"+ques+"_addans_btn").remove();
	$("#ans"+ques).append(str);
	$("#ques"+ques+"_addans").append(btn);
}

function add_ques(el){
	ques = $(el).attr("data-ques");
	next_ques = String(Number.parseInt(ques) + 1);
	str = '<div class="wrap-input100 m-b-16 data-block"><div class="wrap-input100 m-b-16">'+
		'<input class="input100 anser" type="text" name="ques'+next_ques+'" placeholder="Вопрос" required><span class="focus-input100"></span></div>'+
		'<div id="ans'+next_ques+'"><div class="row"><div class="col-1 in-items-center"><strong>1</strong></div><div class="col">'+
		'<div class="wrap-input100 m-b-16"><input class="input100 anser" type="text" name="ques'+next_ques+'_ans1" placeholder="Ответ" required>'+
		'<span class="focus-input100"></span></div></div><div class="col-1 in-items-center">'+
		'<input name="ques'+next_ques+'_rightans" type="radio" value="ans1" checked></div></div><div class="row"><div class="col-1 in-items-center">'+
		'<strong>2</strong></div><div class="col"><div class="wrap-input100 m-b-16">'+
		'<input class="input100 anser" type="text" name="ques'+next_ques+'_ans2" placeholder="Ответ" required><span class="focus-input100"></span>'+
		'</div></div><div class="col-1 in-items-center"><input name="ques'+next_ques+'_rightans" type="radio" value="ans2"></div></div></div><div class="row">'+
		'<div class="col"></div><div class="col-4" id="ques'+next_ques+'_addans"><button type="button" name="ques'+next_ques+'_addans" id="ques'+next_ques+'_addans_btn" '+
		'onclick="add_ans(this);" class="btn btn-secondary my-std-btn in-items-center" data-ques="'+next_ques+'" data-ans="3">Добавить ответ</button>'+
		'</div><div class="col"></div></div></div>'
	btn = '<div class="row" id="ques'+next_ques+'_addques"><div class="col"></div><div class="col-4">'+
		'<button type="button" name="ques'+next_ques+'_addques" id="ques'+next_ques+'_addques_btn" onclick="add_ques(this);" '+
		'class="btn btn-secondary my-std-btn in-items-center" data-ques="'+next_ques+'">Добавить вопрос</button></div>'+
		'<div class="col"></div></div>'
	$("#ques"+ques+"_addques").remove();
	$("#card_body_1").append(str);
	$("#card_body_1").append(btn);
	$("#ques_count").empty();
	$("#ques_count").append(next_ques);
}

function showPercent(el){
	number = Number.parseInt($(el).val());
	queses = Number.parseInt($("#ques_count").html());
	console.log(queses, number);
	console.log(100/queses);
	console.log((100/queses) * number);
	percent = String(Math.round((100/queses) * number));
	console.log(percent);
	console.log("-------------");
	$("#percet").empty();
	$("#percet").append(percent+"%");
}