from migrate.versioning import api
from app import db, app
import os.path
from app.models import User, ROLE_ADMIN, Group, FishAttackOption, FishMail
db.create_all()
if not os.path.exists(app.config['SQLALCHEMY_MIGRATE_REPO']):
    api.create(app.config['SQLALCHEMY_MIGRATE_REPO'], 'database repository')
    api.version_control(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'])
else:
    api.version_control(app.config['SQLALCHEMY_DATABASE_URI'], app.config['SQLALCHEMY_MIGRATE_REPO'], api.version(app.config['SQLALCHEMY_MIGRATE_REPO']))

u = User(username = "Admin", email = "admin@admin.com", password="admin", role = ROLE_ADMIN)
u.set_password("admin")
g = Group(group_name = "Без группы")
all_rows = []
all_rows.append(FishMail(title="Ваше бронирование подтверждено", subject="Ваше бронирование подтверждено", mail_name="Служба бронирования", mail_from="information@official-inbox.com", file_name="0.html"))
all_rows.append(FishMail(title="Посылка отправлена", subject="Посылка отправлена", mail_name="Служба почтовых отправлений", mail_from="service@thedeliverypost.com", file_name="1.html"))
all_rows.append(FishMail(title="Онлайн-опрос для сотрудников: что бы вы улучшили в работе компании", subject="Онлайн-опрос для сотрудников: что бы вы улучшили в работе компании", mail_name="Отдел HR", mail_from="hr@official-inbox.com", file_name="2.html"))
all_rows.append(FishMail(title="Бесплатный вебинар по возвращению налогов", subject="Бесплатный вебинар по возвращению налогов", mail_name="Вебинар Про", mail_from="eduation@internal-mail.com", file_name="3.html"))
all_rows.append(FishMail(title="Зафиксировано опасное нарушение правил дорожного движения", subject="Зафиксировано опасное нарушение правил дорожного движения", mail_name="Condor CCTV", mail_from="security@internal-mail.com", file_name="4.html"))
all_rows.append(FishMail(title="Ваши средства перемещены в частный инвестиционный фонд", subject="Ваши средства перемещены в частный инвестиционный фонд", mail_name="Управление пенсионным вкладом", mail_from="information@internal-mail.com", file_name="5.html"))
all_rows.append(FishMail(title="Умный дом", subject="Умный дом в вашем телефоне", mail_name="Умный дом", mail_from="information@internal-mail.com", file_name="6.html"))
all_rows.append(FishMail(title="Итоги встречи", subject="Благодарю за продуктивную встречу", mail_name="Антон", mail_from="information@internal-mail.com", file_name="7.html"))
all_rows.append(FishMail(title="Корпоративное обучение", subject="Скидки на обучение сотрудников", mail_name="Онлайн образование", mail_from="information@internal-mail.com", file_name="8.html"))
all_rows.append(FishMail(title="Наши боссы на природе", subject="Наши боссы на природе", mail_name="Артем Волчков", mail_from="information@internal-mail.com", file_name="9.html"))
all_rows.append(FishMail(title="Тест на COVID-19 - бесплатно", subject="Тест на вероятность заражения COVID-19 - бесплатно", mail_name="Всемирная Организация Здравоохранения", mail_from="info@stop-covid.center", file_name="10.html"))
all_rows.append(FishMail(title="Вы должны пройти обязательное тестирование на коронавирус", subject="Вы должны пройти обязательное тестирование на коронавирус", mail_name="Оперативный штаб", mail_from="info@stop-covid.center", file_name="11.html"))
all_rows.append(FishMail(title="Нарушение политики использования Интернет", subject="Нарушение корпоративной политики использования интернет-ресурсов", mail_name="Контроль интернет", mail_from="alert@official-inbox.com", file_name="12.html"))
all_rows.append(FishMail(title="Дресс-код", subject="Напоминаем: в нашей компании вводится дресс-код", mail_name="Human Resources", mail_from="hr@official-inbox.com", file_name="13.html"))
all_rows.append(FishMail(title="Нарушение внутреннего распорядка компании", subject="Нарушение внутреннего распорядка компании", mail_name="Оповещение HR", mail_from="warning@official-inbox.com", file_name="14.html"))
all_rows.append(FishMail(title="В нашем отделе новый сотрудник!", subject="В нашем отделе новый сотрудник!", mail_name="Human Resources", mail_from="hr@official-inbox.com", file_name="15.html"))
all_rows.append(FishMail(title="Документ доступен для редактирования", subject="Документ доступен для редактирования", mail_name="Edit online", mail_from="invitation@docs-edit.online", file_name="16.html"))
all_rows.append(FishMail(title="Специальные условия для сотрудников", subject="Специальные условия для сотрудников", mail_name="Скидки в магазине Fashion", mail_from="discount@official-inbox.com", file_name="17.html"))
all_rows.append(FishMail(title="В вашей налоговой декларации выявлены нарушения", subject="Налоговая декларация за 2020 год. В вашей декларации выявлены нарушения", mail_name="Налоговое администрирование", mail_from="info@taxpay365.com", file_name="18.html"))
all_rows.append(FishMail(title="Выплаты в связи с COVID-19", subject="Выплаты в связи с глобальной эпидемией заболевания COVID-19: что полагается вам", mail_name="Оперативный штаб", mail_from="info@stop-covid.center", file_name="19.html"))
all_rows.append(FishMail(title="AliExpress Ваш заказ отправлен", subject="Ваш заказ оплачен и отправлен", mail_name="AliExpress", mail_from="aliexpress@shop-delivery.store", file_name="20.html"))
all_rows.append(FishMail(title="Пароль для обучения", subject="Вам назначено корпоративное обучение", mail_name="Онлайн обучение", mail_from="eduation@internal-mail.com", file_name="21.html"))
all_rows.append(FishMail(title="Ответ на несуществующее письмо", subject="Re: счет на оплату", mail_name="Андрей Смирнов", mail_from="alert@paybill.email", file_name="22.html"))
all_rows.append(FishMail(title="Обновите Adobe Flash Player", subject="Обновите Flash Player на вашем устройстве", mail_name="Adobe Service", mail_from="service@install-soft.me", file_name="23.html"))
all_rows.append(FishMail(title="OneDrive: учетная запись заблокирована", subject="Ваша учетная запись заблокирована", mail_name="OneDrive", mail_from="info@storagealert.work", file_name="24.html"))
all_rows.append(FishMail(title="Обновление страхового плана", subject="Обновлен ваш план медицинского страхования", mail_name="Страховой отдел HR", mail_from="information@internal-mail.com", file_name="25.html"))
all_rows.append(FishMail(title="CustomiseMe План эвакуации", subject="Ознакомиться всем сотрудникам - Новый план эвакуации из здания", mail_name="Отдел безопасности", mail_from="security@internal-mail.com", file_name="26.html"))
all_rows.append(FishMail(title="Новое событие в календаре", subject="Новое событие в календаре", mail_name="Ecalendar", mail_from="service@ecalendar.ws", file_name="27.html"))
all_rows.append(FishMail(title="UPS: неудачная попытка доставки отправления", subject="Неудачная попытка доставки отправления", mail_name="Mail delivery", mail_from="service@thedeliverypost.com", file_name="28.html"))
all_rows.append(FishMail(title="BestTaste: корпоративные скидки на лучшие обеды", subject="Корпоративные скидки на лучшие обеды", mail_name="Best Taste", mail_from="best-taste@business-information.store", file_name="29.html"))
fa = FishAttackOption(user_id=1, logo="temp_logo.png", mails_count=30)
#mails="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]"
for row in all_rows:
	db.session.add(row)
	db.session.flush()
	fa.mails.append(row)
all_rows = []
all_rows.append(u)
all_rows.append(g)
all_rows.append(fa)
db.session.add_all(all_rows)
db.session.commit()