from app import app, db, mail, Message
from app.models import User, Group, FishAttackOption, FishAttack, FishMail
from flask import render_template
import jinja2

import json
import datetime
import random
from threading import Thread


def async_send_mail(app, msg):
	with app.app_context():
		print("Отправка", msg.sender, msg.recipients)
		mail.send(msg)
		print("Отправил")


class MailOperator(object):

	def generateMail(self, user, attack):
		option = json.loads(attack.option)
		mail_ids = option["mails"]
		mail_id = random.choice(mail_ids)
		fish_mail = db.session.query(FishMail).filter(FishMail.id == mail_id).first()
		dates = {
			"today": datetime.datetime.now().strftime("%d/%m/%y"),
			"tomorrow": (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d/%m/%y"),
			"yesterday": (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%d/%m/%y"),
			"dayAfterTomorrow": (datetime.datetime.now() + datetime.timedelta(days=2)).strftime("%d/%m/%y"),
			"dayBeforeYesterday": (datetime.datetime.now() - datetime.timedelta(days=2)).strftime("%d/%m/%y")
		}
		personal = {
			"email": user.email,
			"name": user.first_name + " " + user.second_name
		}
		company = {
			"name": option["company_name"]
		}

		#env = jinja2.Environment(
		#    loader=jinja2.PackageLoader('app','templates')
		#)
		#html = env.get_template("mails/{}".format(fish_mail.file_name))
		#html.render(
		#	dates=dates,
		#	personal=personal,
		#	company=company,
		#	mail_temp="teeeeeest",
		#	logo_name=option["logo"]
		#)

		token = fish_mail.get_fish_token(user.id, attack.id)
		with app.app_context():
			html = render_template(
				"mails/{}".format(fish_mail.file_name),
				dates=dates,
				personal=personal,
				company=company,
				mail_temp=token,
				logo_name=option["logo"]
			)
		return {"html": html, "fish_mail": fish_mail}


	def sendFishMail(self, attack):
		if attack.user_id == None:
			group = db.session.query(Group).filter(Group.id == attack.user_group_id).first()
			for user in group.users:
				html_mail = self.generateMail(user, attack)
				msg = Message(html_mail["fish_mail"].subject,
					recipients=[user.email],
					sender=(html_mail["fish_mail"].mail_name, html_mail["fish_mail"].mail_from),
					html=html_mail["html"])
				thr = Thread(target=async_send_mail,  args=[app,  msg])
				thr.start()
				#mail.send(msg)
		else:
			user = db.session.query(User).filter(User.id == attack.user_id).first()
			html_mail = self.generateMail(user, attack)
			msg = Message(html_mail["fish_mail"].subject,
				recipients=[user.email],
				sender=(html_mail["fish_mail"].mail_name, html_mail["fish_mail"].mail_from),
				html=html_mail["html"])
			thr = Thread(target=async_send_mail,  args=[app,  msg])
			thr.start()
			#mail.send(msg)


	def checkAttacks(self):
		attacks = db.session.query(FishAttack).filter(FishAttack.status == 1, FishAttack.start_time <= datetime.datetime.now()).all()
		for attack in attacks:
			if attack.end_time <= datetime.datetime.now():
				attack.status = 2
				db.session.add(attack)
				continue
			option = json.loads(attack.option)
			option_delay = option["delay"].split(":")
			delay = datetime.timedelta(hours=int(option_delay[0]), minutes=int(option_delay[1]), seconds=int(option_delay[2]))
			if attack.last_mail_time == None or (datetime.datetime.now() - attack.last_mail_time) >= delay:
				self.sendFishMail(attack)
				attack.last_mail_time = datetime.datetime.now()
				db.session.add(attack)
		db.session.commit()