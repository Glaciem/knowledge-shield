# -*- coding: utf-8 -*-
from app import app, db, mail, Message
from app.forms import LoginForm, AddUserForm, AddGroupForm, AddCourseGroupForm, StartFishAttackForm
from app.models import Appointment, TestResult, Test, User, Group, ROLE_USER, Course, CourseGroup, CourseProgress, FishAttackOption, FishAttack, FishMail, ClickStat

from app.libs import FileManager

from flask_login import LoginManager, UserMixin, login_required, login_user, current_user, logout_user
from flask import render_template, flash, redirect, url_for, request, jsonify
from sqlalchemy import func

import uuid
import json
import datetime
import os
from werkzeug.utils  import secure_filename

# ------------------------------------------- 
# ------------------ Pages ------------------
# -------------------------------------------


@app.route('/login/', methods=["POST", "GET"])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('home'))
	login = True
	form = LoginForm()
	if form.validate_on_submit():
		user = db.session.query(User).filter(func.lower(User.username) == form.username.data.lower()).first()
		if user and user.check_password(form.password.data):
			login_user(user, remember=form.remember.data)
			return redirect(url_for('home'))
		flash("Неверный логин или пароль!", 'error')
		return redirect(url_for('login'))
	return render_template('login.html', form=form, login=login)

@app.route('/logout/')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

@app.route('/', methods=["POST", "GET"])
@app.route('/home/', methods=["POST", "GET"])
@login_required
def home():
	users = db.session.query(User).all()
	groups = db.session.query(Group).all()
	groups_data = []
	for group in groups:
		groups_data.append({"group_name": group.group_name, "len": len(group.users)})
	return render_template("index.html", users=users, groups=groups_data)

@app.route('/courses', methods=["POST", "GET"])
@login_required
def courses():
	courses = db.session.query(Course).all()
	all_courses = {
		0: {
			"group": "Без группы",
			"courses": [],
			"test": None
		}
	}
	for course in courses:
		if course.course_groups != []:
			for course_group in course.course_groups:
				try:
					all_courses[course_group.id]["courses"].append(course)
				except:
					tests_r = [test_r for test_r in course_group.test.tests_results if test_r.user_id == current_user.id] if course_group.test != None else None
					tests_r = None if tests_r == [] else tests_r
					if tests_r != None:
						max_result = None
						for test_r in tests_r:
							if max_result == None:
								max_result = test_r
							if test_r.correct > max_result.correct:
								max_result = test_r
						tests_r = max_result
					all_courses[course_group.id] = {
						"group": course_group,
						"courses": [course],
						"test_r": tests_r
					}
		else:
			all_courses[0]["courses"].append(course)
	progress = db.session.query(CourseProgress.course_id).filter(CourseProgress.user_id == current_user.id).all()
	arr_prog = []
	for progres in progress:
		arr_prog.append(progres[0])
	return render_template("courses.html", courses=all_courses, progress=arr_prog)


@app.route('/send_course', methods=["POST", "GET"])
@login_required
def send_course():
	groups = db.session.query(Group).all()
	courses = db.session.query(Course).filter(Course.course_groups == None).all()
	course_groups = db.session.query(CourseGroup).all()
	formData = request.form
	print(formData)
	if "btn_submit" in formData:
		send_groups = []
		send_courses = []
		for data in formData:
			if data[:6] == "group_":
				send_groups.append(int(formData[data]))
			if data[:7] == "course_":
				send_courses.append(int(formData[data]))
		if send_courses == [] and send_groups == []:
			flash("Надо выбрать предмет назначения!", 'error')
			return redirect(url_for('send_course'))
		if formData["group"] == "0":
			user = db.session.query(User).filter(User.username == formData["username"]).first()
			if user:
				a = Appointment(user_id=user.id, deadline=formData["deadline"])
				for send_course in send_courses:
					course = db.session.query(Course).filter(Course.id == send_course).first()
					a.courses.append(course)
				for send_group in send_groups:
					group = db.session.query(CourseGroup).filter(CourseGroup.id == send_group).first()
					a.groups.append(group)
				db.session.add(a)
				db.session.commit()
			else:
				flash("Такого пользователя нет!", 'error')
				return redirect(url_for('send_course'))
		else:
			user_group = db.session.query(Group).filter(Group.id == int(formData["group"])).first()
			if user_group:
				for user in users:
					a = Appointment(user_id=user.id, deadline=formData["deadline"])
					for send_course in send_courses:
						course = db.session.query(Course).filter(Course.id == send_course).first()
						a.courses.append(course)
					for send_group in send_groups:
						group = db.session.query(CourseGroup).filter(CourseGroup.id == send_group).first()
						a.groups.append(group)
					db.session.add(a)
					db.session.commit()
			else:
				flash("Внутренняя ошибка!", 'error')
				return redirect(url_for('send_course'))

	return render_template("send_course.html", groups=groups, courses=courses, course_groups=course_groups)


@app.route('/add_user', methods=["POST", "GET"])
@login_required
def add_user():
	form = AddUserForm()
	groups_db = db.session.query(Group).all()
	groups = []
	for group in groups_db:
		groups.append((group.id, group.group_name))
	form.group.choices = groups
	if form.validate_on_submit():
		user1 = db.session.query(User).filter(func.lower(User.username) == form.username.data.lower()).first()
		user2 = db.session.query(User).filter(func.lower(User.email) == form.email.data.lower()).first()
		if user1:
			flash("Такой логин уже есть!", 'error')
			return redirect(url_for('add_user'))
		if user2:
			flash("Такая почта уже есть!", 'error')
			return redirect(url_for('add_user'))
		uuid_str = str(uuid.uuid4())[:8]
		u = User(username=form.username.data, email=form.email.data, password=uuid_str, first_name=form.first_name.data, second_name=form.second_name.data, third_name=form.third_name.data, role=ROLE_USER, group_id=form.group.data)
		u.set_password(uuid_str)
		db.session.add(u)
		db.session.commit()
		flash("Пользователь {} создан.\nПароль - {}".format(form.username.data, uuid_str), 'error')
		return redirect(url_for('add_user'))
	return render_template("add_user.html", form=form)

@app.route('/add_group', methods=["POST", "GET"])
@login_required
def add_group():
	form = AddGroupForm()
	if form.validate_on_submit():
		group = db.session.query(Group).filter(func.lower(Group.group_name) == form.name.data.lower()).first()
		if group:
			flash("Такая группа уже есть!", 'error')
			return redirect(url_for('add_group'))
		g = Group(group_name=form.name.data)
		db.session.add(g)
		db.session.commit()
		flash("Группа {} создана.".format(form.name.data), 'error')
		return redirect(url_for('add_group'))
	return render_template("add_group.html", form=form)

@app.route('/add_coursegroup', methods=["POST", "GET"])
@login_required
def add_coursegroup():
	form = AddCourseGroupForm()
	if form.validate_on_submit():
		group = db.session.query(CourseGroup).filter(func.lower(CourseGroup.group_title) == form.name.data.lower()).first()
		if group:
			flash("Такая группа уже есть!", 'error')
			return redirect(url_for('add_coursegroup'))
		g = CourseGroup(group_title=form.name.data, description=form.description.data)
		db.session.add(g)
		db.session.commit()
		flash("Группа {} создана.".format(form.name.data), 'error')
		return redirect(url_for('add_coursegroup'))
	return render_template("add_coursegroup.html", form=form)

@app.route('/add_course', methods=["POST", "GET"])
@login_required
def add_course():
	groups_db = db.session.query(CourseGroup).all()
	formData = request.form
	if "btn_submit" in formData:
		if formData["course_title"] == "":
			flash("Название курса не может быть пустым", 'error')
			return redirect(url_for('add_course'))
		course = db.session.query(Course).filter(func.lower(Course.course_title) == formData["course_title"].lower()).first()
		if course:
			flash("Такое имя курса уже занято!", 'error')
			return redirect(url_for('add_course'))

		pages = {}
		last_page = ""
		input_names = []
		for item in formData:
			if item != "btn_submit" and item != "course_title" and item != "description" and item != "group":
				splited = item.split("_")
				input_names.append(splited)
				if splited[0] != last_page:
					last_page = splited[0]
					pages[last_page] = {}

		count = 0
		last_block = ""
		for page in pages:
			for name in input_names:
				if name[0] == page:
					if last_block != name[1]:
						last_block = name[1]
						pages[page][last_block] = {}
			for block in pages[page]:
				for name in input_names:
					if name[0] == page and name[1] == block:
						pages[page][block][name[2]] = formData["{}_{}_{}".format(name[0], name[1], name[2])]

		course = {
			"name": formData["course_title"],
			"description": formData["description"],
			"pages": []
		}
		page_count = 0
		for page in pages:
			page_count+=1
			json_page = {
				"block_count": len(pages[page]),
				"blocks":[]
			}
			count = 0
			for block in pages[page]:
				if count+1 == len(pages[page]):
					if page_count == len(pages):
						submit = "end_course"
					else:
						submit = "next_page"
				else:
					submit = "next_block"
				img = request.files["{}_{}_img".format(page, block)]
				img_name = secure_filename(img.filename)
				#print(url_for("static", filename="images/courses/"))
				#print(os.listdir(path="."))
				if bool(img_name):
					img_name = datetime.datetime.now().strftime("%d-%m-%y_%H-%M-%S")+"_"+img_name
					img.save(os.path.join("app/static/images/courses/", img_name))
				json_block = {
					"block_number": count,
					"type": pages[page][block]["radio"],
					"payload": {
						"text": pages[page][block]["text"],
						#"img": pages[page][block]["img"],
						"img": img_name,
						"submit": submit
					}
				}
				json_page["blocks"].append(json_block)
				count+=1
			course["pages"].append(json_page)

		c = Course(course_title=formData["course_title"], description=formData["description"], structure=json.dumps(course))
		if formData["group"] != "0":
			group = db.session.query(CourseGroup).filter(CourseGroup.id == int(formData["group"])).first()
			c.course_groups.append(group)
		db.session.add(c)
		db.session.commit()

		flash("Курс {} создан.".format(formData["course_title"]), 'error')
		return redirect(url_for('add_course'))
	return render_template("add_course.html", groups_db=groups_db)


@app.route('/add_test', methods=["POST", "GET"])
@login_required
def add_test():
	groups_db = db.session.query(CourseGroup).filter(CourseGroup.test == None).all()
	formData = request.form
	if "btn_submit" in formData:
		if formData["test_title"] == "":
			flash("Название теста не может быть пустым", 'error')
			return redirect(url_for('add_test'))
		test = db.session.query(Test).filter(func.lower(Test.test_title) == formData["test_title"].lower()).first()
		if test:
			flash("Такое имя теста уже занято!", 'error')
			return redirect(url_for('add_course'))
		input_names=[]
		max_ques=0
		structure={}
		for item in formData:
			if item != "btn_submit" and item != "test_title" and item != "description" and item != "group" and item != "porog":
				splited = item.split("_")
				if len(splited) != 1:
					input_names.append(splited)
				else:
					structure[splited[0][4:]]={"ques": formData[splited[0]], "ans": {}}
		for ques_id in structure:
			for input_ in input_names:
				if input_[0][4:] == ques_id:
					if input_[1] != "rightans":
						structure[ques_id]["ans"][input_[1][3:]] = formData["ques{}_{}".format(ques_id, input_[1])]
					else:
						structure[ques_id]["rightans"] = formData["ques{}_{}".format(ques_id, formData["ques{}_rightans".format(ques_id)])] 
		t = Test(test_title=formData["test_title"], group_id=formData["group"], structure=json.dumps(structure), porog=int(formData["porog"]), max_ans=len(structure))
		db.session.add(t)
		db.session.commit()

		flash("Тест {} создан.".format(formData["test_title"]), 'error')
		return redirect(url_for('add_test'))
	return render_template("add_test.html", groups_db=groups_db)

@app.route('/tests/<test_id>', methods=["POST", "GET"])
@login_required
def test(test_id):
	test = db.session.query(Test).filter(Test.id == int(test_id)).first()
	if not test:
		return redirect(url_for('home'))
	if db.session.query(TestResult).filter(TestResult.user_id == current_user.id, TestResult.result == 1, TestResult.test_id == int(test_id)).first():
		flash("Вы уже прошли этот тест!", 'error')
		return redirect(url_for('courses'))
	formData = request.form
	if "btn_submit" in formData:
		correct = 0
		klient_structure = {}
		for ques in formData:
			if ques != "btn_submit":
				klient_structure[ques] = formData[ques]
			if ques != "btn_submit" and formData[ques] == json.loads(test.structure)[ques]["rightans"]:
				correct+=1
		if test.porog > correct:
			tr = TestResult(test_id=test.id, user_id=current_user.id, group_id=test.group_id, result=0, correct=correct, structure=json.dumps(klient_structure))
			flash("Тест не пройден!", 'error')
		else:
			tr = TestResult(test_id=test.id, user_id=current_user.id, group_id=test.group_id, result=1, correct=correct, structure=json.dumps(klient_structure))
			flash("Тест пройден!", 'error')
		db.session.add(tr)
		db.session.commit()
		return redirect(url_for('courses'))
	return render_template("test.html", course_title=test.test_title, structure=json.loads(test.structure))

@app.route('/tests/<test_id>/result', methods=["POST", "GET"])
@login_required
def test_result(test_id):
	test_r = db.session.query(TestResult).filter(TestResult.user_id == current_user.id, TestResult.result == 1, TestResult.test_id == int(test_id)).first()
	return render_template("test_result.html", course_title=test_r.tests.test_title, test_r=json.loads(test_r.structure), test=json.loads(test_r.tests.structure))


@app.route('/mail_temlates', methods=["POST", "GET"])
@login_required
def mail_temlates():
	fish_mails = db.session.query(FishMail).all()
	return render_template("mail_templates.html", fish_mails=fish_mails)

@app.route('/fish_options', methods=["POST", "GET"])
@login_required
def fish_options():
	formData = request.form
	if "btn_submit" in formData:
		option = db.session.query(FishAttackOption).filter(FishAttackOption.user_id == current_user.id).first()
		logo = request.files["logo"]
		logo_name = secure_filename(logo.filename)
		if bool(logo_name):
			logo_name = datetime.datetime.now().strftime("%d-%m-%y_%H-%M-%S")+"_"+logo_name
			logo.save(os.path.join("app/static/images/logos/", logo_name))
			option.logo = logo_name
		option.inbox = formData["domen"]
		option.company_name = formData["company_name"]
		option.delay = "{}:{}:{}".format(formData["hour"], formData["minutes"], formData["seconds"])
		new_mails=[]
		for check in formData:
			if check[:5] == "check":
				new_mails.append(db.session.query(FishMail).filter(FishMail.id == int(formData[check])).first())
		option.mails = new_mails
		option.mails_count = len(new_mails)
		db.session.add(option)
		db.session.commit()
		flash("Настройки изменены", 'error')
	#print(formData)
	option = db.session.query(FishAttackOption).filter(FishAttackOption.user_id == current_user.id).first()
	details = {
		"logo": option.logo,
		"inbox": option.inbox,
		"company_name": option.company_name,
		"mails_count": option.mails_count,
		"mails": option.mails,
		"delay": option.delay.split(":")
	}
	fish_mails = db.session.query(FishMail).all()
	return render_template("fish_options.html", option=details, fish_mails=fish_mails)


@app.route('/fish_start', methods=["POST", "GET"])
@login_required
def fish_start():
	form = StartFishAttackForm()
	groups_db = db.session.query(Group).all()
	groups = [(0, "Не выбрано")]
	for group in groups_db:
		groups.append((group.id, group.group_name))
	form.group.choices = groups
	if form.validate_on_submit():
		fish_option = db.session.query(FishAttackOption).filter(FishAttackOption.user_id == current_user.id).first()
		mails = []
		for mail in fish_option.mails:
			mails.append(mail.id)
		option = {
			"company_name": fish_option.company_name,
			"logo": fish_option.logo,
			"inbox": fish_option.inbox,
			"delay": fish_option.delay,
			"mails": mails
		}
		option = json.dumps(option)
		if form.username.data != "":
			user = db.session.query(User).filter(func.lower(User.username) == form.username.data.lower()).first()
			if not user:
				flash("Такого пользователя не существует!", 'error')
				return redirect(url_for('fish_start'))
			fa = FishAttack(attack_title=form.title.data, user_id=user.id, start_time=form.start_time.data,\
				end_time=form.end_time.data, option=option)
		elif form.group.data != 0:
			fa = FishAttack(attack_title=form.title.data, user_group_id=form.group.data, start_time=form.start_time.data,\
				end_time=form.end_time.data, option=option)
		else:
			flash("Нужно выбрать цель атаки!", 'error')
			return redirect(url_for('fish_start'))
		db.session.add(fa)
		db.session.commit()
	fish_attacks = db.session.query(FishAttack).all()
	return render_template("fish_attack.html", fish_attacks=fish_attacks, form=form)


@app.route('/fish_click/<mail_temp>', methods=["POST", "GET"])
def fish_click(mail_temp):
	token_data = FishMail().verify_token(mail_temp)
	if token_data:
		cs = ClickStat(user_id=token_data[0].id, attack_id=token_data[1].id, mail_id=token_data[2].id)
		db.session.add(cs)
		db.session.commit()
	else:
		return redirect(url_for('home'))
	return render_template("fish_click.html")


@app.route('/courses/<course_id>', methods=["POST", "GET"])
@login_required
def course(course_id):
	course = db.session.query(Course).filter(Course.id == course_id).first()
	progress = db.session.query(CourseProgress).filter(CourseProgress.course_id == course.id, CourseProgress.user_id == current_user.id).first()
	if progress:
		db.session.delete(progress)
		db.session.commit()
	page = 0
	return render_template("work_base.html", course_title=course.course_title, course=course, page=page)


# ------------------------------------------- 
# ------------------- API -------------------
# -------------------------------------------

@app.route('/api/get_course_page', methods=["GET"])
@login_required
def api_get_course_page():
	try:
		course_id = int(request.args.get('course_id'))
		page = int(request.args.get('page'))
	except:
		print("Не взял арги")
	course = db.session.query(Course).filter(Course.id == course_id).first()
	
	json_course = json.loads(course.structure)
	html_blocks = []
	for block in json_course["pages"][page]["blocks"]:
		print(block["payload"]["submit"])
		html = render_template(
			"courses/{}.html".format(block["type"]),
			course_title=course.course_title,
			text=block["payload"]["text"],
			img_url=url_for("static", filename="images/courses/{}".format(block["payload"]["img"])),
			page=page,
			submit=block["payload"]["submit"]
		)
		#str_html = str(html)
		html_blocks.append(html)
	return jsonify(html_blocks)
	#return render_template("courses/course_temp.html", blocks=html_blocks)

@app.route('/api/add_course', methods=["POST"])
@login_required
def api_add_course():
	content = request.get_json()
	json_content = json.dumps(content["pages"])
	temp_course = db.session.query(Course).filter(func.lower(Course.course_title) == content["name"].lower()).first()
	if temp_course:
		jsonify({"result": False})
	c = Course(course_title=content["name"], description=content["description"], structure=json_content)
	db.session.add(c)
	db.session.commit()
	return jsonify({"result": True})


@app.route('/api/end_course', methods=["POST"])
@login_required
def api_end_course():
	content = request.get_json()
	cp = CourseProgress(course_id=content["course_id"], user_id=current_user.id)
	db.session.add(cp)
	db.session.commit()
	return jsonify({"result": True})

@app.route('/api/get_fish_mail', methods=["GET"])
@login_required
def get_fish_mail():
	try:
		mail_id = int(request.args.get('mail_id'))
	except:
		print("Не взял арги")
	fish_mail = db.session.query(FishMail).filter(FishMail.id == mail_id).first()
	attack_option = db.session.query(FishAttackOption).filter(FishAttackOption.user_id == current_user.id).first()
	dates = {
		"today": datetime.datetime.now().strftime("%d/%m/%y"),
		"tomorrow": (datetime.datetime.now() + datetime.timedelta(days=1)).strftime("%d/%m/%y"),
		"yesterday": (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%d/%m/%y"),
		"dayAfterTomorrow": (datetime.datetime.now() + datetime.timedelta(days=2)).strftime("%d/%m/%y"),
		"dayBeforeYesterday": (datetime.datetime.now() - datetime.timedelta(days=2)).strftime("%d/%m/%y")
	}
	personal = {
		"email": "почта_сотрудника",
		"name": "имя_сотрудника"
	}
	company = {
		"name": attack_option.company_name
	}
	html = render_template(
		"mails/{}".format(fish_mail.file_name),
		dates=dates,
		personal=personal,
		company=company,
		mail_temp="teeeeeest",
		logo_name=attack_option.logo
	)
	return jsonify({"result": True, "html": html})